<?php

namespace CoreBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CoreControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $form = $crawler->selectButton('corebundle_contactform[submit]')->form();

        $form['corebundle_contactform[secondName]'] = 'ROBERT';
        $form['corebundle_contactform[email]'] = 'test@outlook.fr';
        $form['corebundle_contactform[question]'] = 'Ceci est un test ?';

        $crawler = $client->submit($form);

        try {
            $this->assertTrue($client->getResponse()->isRedirect());
            $client->followRedirect();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }

    }
}
