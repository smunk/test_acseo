<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ContactForm
 *
 * @ORM\Table(name="contact_form")
 * @ORM\Entity(repositoryClass="CoreBundle\Repository\ContactFormRepository")
 */
class ContactForm
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="secondName", type="string", length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max="255",
     *     maxMessage="La valeur est trop longue. Elle ne doit pas dépasser {{ limit }} caractères."
     * )
     */
    private $secondName;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255, nullable=true)
     *
     * @Assert\Length(
     *     max="255",
     *     maxMessage="La valeur est trop longue. Elle ne doit pas dépasser {{ limit }} caractères."
     * )
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     *
     * @Assert\Email(
     *     checkMX = true,
     *     message="L'adresse email n'est pas valide."
     * )
     * @Assert\Length(
     *     max="255",
     *     maxMessage="La valeur est trop longue. Elle ne doit pas dépasser {{ limit }} caractères."
     * )
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="text")
     *
     * @Assert\NotBlank()
     */
    private $question;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     *
     * @Assert\DateTime()
     */
    private $createdAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="ready", type="boolean", nullable=true)
     *
     */
    private $ready;

    /**
     * ContactForm constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->ready = false;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set secondName
     *
     * @param string $secondName
     *
     * @return ContactForm
     */
    public function setSecondName($secondName)
    {
        $this->secondName = $secondName;

        return $this;
    }

    /**
     * Get secondName
     *
     * @return string
     */
    public function getSecondName()
    {
        return $this->secondName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return ContactForm
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return ContactForm
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return ContactForm
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ContactForm
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set ready
     *
     * @param bool $ready
     *
     * @return ContactForm
     */
    public function setReady($ready)
    {
        $this->ready = $ready;

        return $this;
    }

    /**
     * Get ready
     *
     * @return bool
     */
    public function getReady()
    {
        return $this->ready;
    }
}

