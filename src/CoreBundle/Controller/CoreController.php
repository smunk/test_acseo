<?php

namespace CoreBundle\Controller;

use CoreBundle\Entity\ContactForm;
use CoreBundle\Form\ContactFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CoreController
 * @package CoreBundle\Controller
 */
class CoreController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function contactAction(Request $request)
    {
        $contact = new ContactForm();

        $contactForm = $this->createForm(ContactFormType::class, $contact);
        $contactForm->handleRequest($request);

        if ($contactForm->isSubmitted() && $contactForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();
            $this->addFlash('success', 'Votre message a bien été envoyé.');
            return $this->redirectToRoute('core_contactpage');
        }

        return $this->render('@Core/contact.html.twig', ['contactForm' => $contactForm->createView()]);
    }
}
