<?php

namespace AdminBundle\Controller;

use CoreBundle\Entity\ContactForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AdminController
 * @package AdminBundle\Controller
 */
class AdminController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function indexAction(Request $request)
    {
        if ($this->isGranted('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();

            if ($request->isXmlHttpRequest()) {
                $id = $request->get('id');
                if (isset($id)) {
                    /**
                     * @var ContactForm $message
                     */
                    $message = $em->getRepository('CoreBundle:ContactForm')->find($id);

                    if ($message) {
                        $message->setReady(!$message->getReady());
                        $em->persist($message);
                        $em->flush();
                        return new Response('Le status du message a été mis à jour.', 200);
                    }
                }

                return new Response("Une erreur est survenue", 400);
            }

            /**
             * @var ContactForm $messages
             */
            $messages = $em->getRepository('CoreBundle:ContactForm')->findAllMessages();

            return $this->render('@Admin/Default/index.html.twig', [
                'messages' => $messages,
                ]);
        }

        return $this->redirectToRoute('core_contactpage');
    }
}
