$(function() {
    $('.ready').click(function () {
        let val = $(this).attr('value');
        let id = $(this).attr('id');
        let checkbox = $(this);
        let pathname = window.location.pathname;
        let count = $('#count');
        let valCount = $(count).text();


        $.ajax({
           method: "POST",
           url: pathname,
            data: { id, val }
        })
            .done(function (msg) {
                if ($(checkbox).is(':checked')) {
                    /* Remove 1 if checked */
                    $(count).text(parseInt(valCount) - 1);
                } else {
                    /* Add 1 if unchecked */
                    $(count).text(parseInt(valCount) + 1);
                }
                alert(msg);
            })
        ;
    });
});